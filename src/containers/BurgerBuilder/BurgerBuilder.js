import React, { Component } from 'react';
import Aux from '../../hoc/Aux';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal'
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import axios from 'axios';
import Spinner from '../../components/UI/Spinner/Spinner'
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'
import { connect } from 'react-redux'
import * as actions from '../../store/actions/index';

/*const INGREDIENTS_PRICE ={
	salad: 0.6,
	meat: 2,
	bacon: 1,
	cheese: 0.8
}*/

class BurgerBuilder extends Component {

	state = {
		//ingredients: null,
		//totalPrice: 0,
		//purchasable: false,
		purchasing: false
		//loading: false,																																																																																																																						
		//error: false
	}

	componentDidMount () {
        /*axios.get( 'https://myburger-411c9.firebaseio.com/ingredients.json' )
            .then( response => {
                this.setState( { ingredients: response.data } );
            } )
            .catch( error => {
                this.setState( { error: true } );
            } );*/
        this.props.onInitIngredients();																																																									
    }

	updatePurchaseState(ingredients) {
		const sum = Object.keys( ingredients )
            .map( igKey => {
                return ingredients[igKey];
            } )
            .reduce( ( sum, el ) => {
                return sum + el;
            }, 0 );
        //this.setState( { purchasable: sum > 0 } );
        return sum>0;
	}

	/*addIngredienthandler = (type) => {
		const oldCount = this.state.ingredients[type];
		const newCount = oldCount + 1;
		const oldPrice = this.state.totalPrice;
		const newPrice = oldPrice + INGREDIENTS_PRICE[type];
		const updatedIngredients = {
			...this.state.ingredients
		}
		updatedIngredients[type] = newCount;
		this.setState({
			ingredients: updatedIngredients,
			totalPrice: newPrice
		});
		this.updatePurchaseState(updatedIngredients);
	}

	removeIngredienthandler = (type) => {
		const oldCount = this.state.ingredients[type];
		if (oldCount <= 0) {
			return;
		}
		const newCount = oldCount - 1;
		const oldPrice = this.state.totalPrice;
		const newPrice = oldPrice - INGREDIENTS_PRICE[type];
		const updatedIngredients = {
			...this.state.ingredients
		}
		updatedIngredients[type] = newCount;
		this.setState({
			ingredients: updatedIngredients,
			totalPrice: newPrice
		});
		this.updatePurchaseState(updatedIngredients);
	}*/

	purchaseHandler= () => {
		if (this.props.isAuthenticated) {
			this.setState({purchasing: true});
		} else {
			this.props.onSetAuthRedirectPath('/checkout');
			this.props.history.push('/auth');
		}
	}

	purchaseCancelHandler= () => {
		this.setState({purchasing: false});
	}

	purchaseContinueHandler = () => {
		/*const queryParams = [];
        for (let i in this.state.ingredients) {
            queryParams.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i]));
        }
        queryParams.push('price=' + this.state.totalPrice);
        const queryString = queryParams.join('&');
        this.props.history.push({
            pathname: '/checkout',
            search: '?' + queryString
        });*/
        this.props.onInitPurchase();
        this.props.history.push('/checkout');
	}

	render() {
		const disabledInfo = {
			//...this.state.ingredients
			...this.props.ings
		}
		for (let key in disabledInfo) {
			disabledInfo[key] = disabledInfo[key] <=0 //return true or false
		}

		let orderSummary = null;
        let burger = this.props.error ? <p>Ingredients can't be loaded!</p> : <Spinner />;
        if ( /*this.state.ingredients*/ this.props.ings ) {
            burger = (
                <Aux>
                    <Burger ingredients={/*this.state.ingredients*/ this.props.ings} />
                    <BuildControls ingredientAdded={this.props.onIngredientAdded}
					 	ingredientRemoved={this.props.onIngredientRemoved}
					 	disabled={disabledInfo}
					 	price={this.props.price}
					 	ordered={this.purchaseHandler}
					 	isAuth={this.props.isAuthenticated}
					 	purchasable={this.updatePurchaseState(this.props.ings)} />
                </Aux>
            );
            orderSummary = <OrderSummary ingredients={this.props.ings}
							  purchaseCanceled={this.purchaseCancelHandler}
							  purchaseContinued={this.purchaseContinueHandler}
							  price={this.props.price} />
        } 
		/*if (this.state.loading) {
			orderSummary = <Spinner />
		}*/

		return (
			<Aux>
				<Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
					{orderSummary}
				</Modal>
				{burger}
			</Aux>
		);
	}
}

const mapStateToProps = state => {
	return {
		ings: state.burgerBuilder.ingredients,
		price: state.burgerBuilder.totalPrice,
		error: state.burgerBuilder.error,
		isAuthenticated: state.auth.token != null
	};
}

const mapDispatchToProps = dispatch => {
	return {
		onIngredientAdded: (ingName) => dispatch(actions.addIngredient(ingName)),
		onIngredientRemoved: (ingName) => dispatch(actions.removeIngredient(ingName)),
		onInitIngredients: (ingName) => dispatch(actions.initIngredients(ingName)),
		onInitPurchase: () => dispatch(actions.purchaseInit()),
		onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder , axios));