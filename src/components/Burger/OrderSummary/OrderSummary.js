import React from 'react';
import Aux from '../../../hoc/Aux';
import Button from '../../UI/Button/Button';

const orderSummary = (props) => {
	const ingredientSummary = Object.keys( props.ingredients )
            .map( igKey => {
                return (
                	<li key={igKey}>
                		<span style={{textTransform: 'capitalize'}}>{igKey}</span>: {props.ingredients[igKey]}
                	</li>
                );
            });

	return(
		<Aux>
			<h3>Your Order Summary</h3>
			<p>The ingredients are:</p>
			<ul>
				{ingredientSummary}
			</ul>
			<h2>Yout total price for the order is: {props.price.toFixed(2)}</h2>
			<p>Continue to checkout?</p>
			<Button btnType="Danger" clicked={props.purchaseCanceled}>Cancel</Button>
			<Button btnType="Success" clicked={props.purchaseContinued}>Continue</Button>
		</Aux>
	);
}

export default orderSummary;